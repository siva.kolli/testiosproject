package com.iOS.GitLabIOSAppium;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;

public class BaseIOS {
	public static IOSDriver<IOSElement> iOSDriver;
	
	@Test
	public void test() {
		System.out.println("app launched");
	}

	@BeforeSuite
	public void startServer() throws MalformedURLException {
		DesiredCapabilities dc = new DesiredCapabilities();
		dc.setCapability("platformName", "iOS");
		dc.setCapability("platformVersion", "14.4");
		dc.setCapability("automationName", "XCUITest");
		dc.setCapability("app", System.getProperty("user.dir") + "/mobileApps/UICatalog.app");
//		dc.setCapability("bundleId", "com.baserythm.Teladentistry");
//		dc.setCapability("udid", "134404177859263762c54e19879df386fc1f7573");
		//dc.setCapability("deviceName", "iPad (7th generation)");
		dc.setCapability("deviceName", "iPhone 12");
		dc.setCapability("autoGrantPermissions", "true");
		dc.setCapability("autoAcceptAlerts", "true");
		dc.setCapability("noReset", true);
		dc.setCapability("commandTimeOut", 300);
		dc.setCapability("useNewWDA", true);
		dc.setCapability("simulatorStartupTimeout", 240000);
		iOSDriver = new IOSDriver<IOSElement>(new URL("http://127.0.0.1:4723/wd/hub"), dc);

		iOSDriver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@AfterSuite
	public void killServer() {
		iOSDriver.quit();
	}

}
