package com.iOS.GitLabIOSAppium;

import java.time.Duration;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class UICatalogPage {
	//AppiumDriver<MobileElement> appiumDriver;
	
	public UICatalogPage(IOSDriver<IOSElement> appiumDriver) {
		PageFactory.initElements(new AppiumFieldDecorator(appiumDriver, Duration.ofSeconds(10)), this);
		//this.appiumDriver = appiumDriver;
	}
	
	@AndroidFindBy(id = "yesPhoneState")
	@iOSXCUITFindBy(xpath = "//*[@name='AAPLActivityIndicatorViewController']")
	private MobileElement continueButtonWithID;
	
	
	@iOSXCUITFindBy(accessibility = "AAPLImageViewController")
	private MobileElement datePicker;
	
	@iOSXCUITFindBy(xpath = "(//*[@type='XCUIElementTypePickerWheel'])[3]")
	private MobileElement timeValue;
	
	public void tapElement() {
		continueButtonWithID.click();
	}
	
	
	public void clickOnDatePicker() {
		
		datePicker.click();
		
		timeValue.sendKeys("15");

	}
	
	
}
